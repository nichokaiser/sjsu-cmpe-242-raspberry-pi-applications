import board
import busio
import time
import math
import Adafruit_LSM303
import RPi.GPIO as GPIO
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

i2c = busio.I2C(board.SCL, board.SDA)
ads = ADS.ADS1115(i2c)
lsm303 = Adafruit_LSM303.LSM303()

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT) #STEP pin (Pi pin 12, BCM pin 18)
GPIO.setup(23, GPIO.OUT) #DIR pin (Pi pin 16, BCM pin 23)

pwm_pin = GPIO.PWM(18, 50) #channel=18, frequency=50Hz
pwm_pin.start(0)
pwm_pin.ChangeDutyCycle(0)
    
try:
    while 1:
        accel, mag = lsm303.read()
        accel_x, accel_y, accel_z = accel
        mag_x, mag_y, mag_z = mag
        heading = (math.atan2(mag_y, mag_x) * 180) / math.pi
        if heading < 0:
            heading = 360 + heading
        print('Accel X={0}, Accel Y={1}, Accel Z={2}, Mag X={3}, Mag Y={4}, Mag Z={5}, Heading={6}\N{DEGREE SIGN}'.format(accel_x, accel_y, accel_z, mag_x, mag_y, mag_z, heading))
        
        chan = AnalogIn(ads, ADS.P0)
        scaled_value = 19859.3939*chan.voltage
        scaled_value = int(scaled_value)
        
        if (scaled_value < 0):
            scaled_value = 0
        if (scaled_value > 65536):
            scaled_value = 65536
        print("ADC Value=", scaled_value, "ADC Voltage=", chan.voltage)
        
        if ((scaled_value >= 0) and (scaled_value <= 29999)):
            GPIO.output(23, GPIO.LOW) #set DIR to counter clockwise
            pwm_pin.ChangeDutyCycle(50)
            frequency = (-0.3333*scaled_value) + (1.0*pow(10,4))
            pwm_pin.ChangeFrequency(frequency)
            print("PWM Frequency=", frequency)
        elif ((scaled_value >= 30000) and (scaled_value <= 35000)):
            pwm_pin.ChangeDutyCycle(0)
            print("Duty Cycle=0, PWM Frequency=1")
        elif ((scaled_value >= 35001) and (scaled_value <= 65536)):
            GPIO.output(23, GPIO.HIGH) #set DIR to clockwise
            pwm_pin.ChangeDutyCycle(50)
            frequency = (0.3275*scaled_value) - (1.146*pow(10,4))
            pwm_pin.ChangeFrequency(frequency)
            print("PWM Frequency=", frequency)

        time.sleep(1) 

except KeyboardInterrupt:
    pass
pwm_pin.stop()
GPIO.cleanup()