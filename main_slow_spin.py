import board
import busio
import time
import math
import Adafruit_LSM303
import RPi.GPIO as GPIO
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn

i2c = busio.I2C(board.SCL, board.SDA)
ads = ADS.ADS1115(i2c)
lsm303 = Adafruit_LSM303.LSM303()

GPIO.setmode(GPIO.BCM)
GPIO.setup(18, GPIO.OUT) #STEP pin (Pi pin 12, BCM pin 18)
GPIO.setup(23, GPIO.OUT) #DIR pin (Pi pin 16, BCM pin 23)

GPIO.output(23, GPIO.HIGH) #set DIR to clockwise
pwm_pin = GPIO.PWM(18, 20) #channel=18, frequency=20Hz
pwm_pin.start(0)
pwm_pin.ChangeDutyCycle(50)
    
try:
    while 1:
        accel, mag = lsm303.read()
        accel_x, accel_y, accel_z = accel
        mag_x, mag_y, mag_z = mag
        heading = (math.atan2(mag_y, mag_x) * 180) / math.pi
        if heading < 0:
            heading = 360 + heading
        print('Accel X={0}, Accel Y={1}, Accel Z={2}, Mag X={3}, Mag Y={4}, Mag Z={5}, Heading={6}\N{DEGREE SIGN}'.format(accel_x, accel_y, accel_z, mag_x, mag_y, mag_z, heading))
        
        time.sleep(1) 

except KeyboardInterrupt:
    pass

GPIO.output(23, GPIO.LOW) #set DIR to counter clockwise
 
try:
    while 1:
        accel, mag = lsm303.read()
        accel_x, accel_y, accel_z = accel
        mag_x, mag_y, mag_z = mag
        heading = (math.atan2(mag_y, mag_x) * 180) / math.pi
        if heading < 0:
            heading = 360 + heading
        print('Accel X={0}, Accel Y={1}, Accel Z={2}, Mag X={3}, Mag Y={4}, Mag Z={5}, Heading={6}\N{DEGREE SIGN}'.format(accel_x, accel_y, accel_z, mag_x, mag_y, mag_z, heading))
                
        time.sleep(1) 

except KeyboardInterrupt:
    pass
pwm_pin.stop()
GPIO.cleanup()