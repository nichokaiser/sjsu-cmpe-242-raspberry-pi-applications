# SJSU CMPE 242 Raspberry Pi Applications

The goal of this project was to interface an ADS1115 16-bit 4 channel ADC module, A3967 stepper motor driver, NEMA 17 stepper motor, and an LSM303DLHC accelerometer and magnetometer module with a Raspberry Pi 3 in order to read an input analog voltage over an I2C interface, use PWM to actuate the stepper motor based on this analog voltage value, and then determine the heading angle of the stepper motor’s shaft.

## Getting Started

Take a look at the project report (pdf document) in order to see how to connect all components to the Raspberry Pi. The report contains a list of all the parts you will need, and schematics that show how everything should be connected.

## Running the Source Code

Instructions for running the source code "main.py":

*Note:"main_slow_spin.py" is only used for turning the motor slowly to see the heading values change on the console.

The following steps assume that I2C is already enabled on the Pi. Please refer to this link to enable I2C "https://learn.adafruit.com/adafruits-raspberry-pi-lesson-4-gpio-setup/configuring-i2c"

1. The Adafruit Blinka library was used to connect the ADS1115 ADC module to the Pi 3 and so it needs to be downloaded on the Pi first. It's included in this zip file as "Adafruit_Blinka-master.zip" or it can be found on GitHub https://github.com/adafruit/Adafruit_Blinka

2. Once the Adafruit Blinka library is installed, run this command on the Pi 3's terminal "sudo pip3 install adafruit-circuitpython-ads1x15"
Note: if default Python is version 3, you may need to run this instead "sudo pip install adafruit-circuitpython-ads1x15"

3. A folder called "Adafruit_Python_ADS1x15" should now be in the Pi's "Filesystem Root". Create a new folder called "CMPE243_final_project" in the Pi's "Filesystem Root", and drag the "Adafruit_Python_ADS1x15" folder into it. 

4. The following libraries also need to be downloaded and dragged into the "CMPE243_final_project" folder for the LSM303DLHC module:
- Adafruit Python GPIO: https://github.com/adafruit/Adafruit_Python_GPIO
- Adafruit Python PureIO: https://github.com/adafruit/Adafruit_Python_PureIO
- Adafruit Python LSM303: https://github.com/adafruit/Adafruit_Python_LSM303
*Note: follow the README.md file instructions in the Github links above to install each library. If there are none, just download the entire Github repository.

5. Put the source code files "main.py" and "main_slow_spin.py" into the "CMPE243_final_project" folder and open and run using Thonny Python IDE.
